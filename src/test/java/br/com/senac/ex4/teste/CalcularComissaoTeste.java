
package br.com.senac.ex4.teste;

import br.com.senac.ex4.CalcularComissao;
import br.com.senac.ex4.Venda;
import static org.junit.Assert.*;
import org.junit.Test;


public class CalcularComissaoTeste {
    
@Test
    public void deveCalcularComissao(){
    
    CalcularComissao calculadora = new CalcularComissao();
    
    double resultado = calculadora.calcular(new Venda(25, 8));
    assertEquals(10, resultado, 0.001);
    
    
}
}

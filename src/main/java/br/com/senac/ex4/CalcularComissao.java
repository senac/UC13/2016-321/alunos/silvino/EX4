package br.com.senac.ex4;

public class CalcularComissao {

    

    public double calcular(Venda venda) {

        double valorFinal = venda.getQuantidadeVendida() * venda.getPrecoUnitario();
        double comissao = valorFinal * 0.05;

        return comissao;
    }

}
